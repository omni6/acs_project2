const fs = require('fs');
const process = require('process');
const { execSync } = require('child_process');

const aws = require('aws-sdk');

const s3 = new aws.S3({ apiVersion: '2006-03-01' });

const { 
	GITLAB_TOKEN_NAME, 
	GITLAB_TOKEN, 
	GITLAB_USERNAME, 
	GITLAB_EMAIL, 
	GITLAB_REPO_URL 
} = process.env;

const t = GITLAB_REPO_URL.substring(GITLAB_REPO_URL.lastIndexOf('/') + 1);
const repoName = t.split('.')[0]; 

const getS3Object = async (event) => {
	const s3object = event.Records[0].s3
	const bucket = s3object.bucket.name;
	const key = decodeURIComponent(s3object.object.key.replace(/\+/g, ' '));
	let params = {
		Bucket: bucket,
		Key: key
	};
	
	console.log(`Writing ${key} to /tmp/${repoName}/${key}`);
	
	const out = fs.createWriteStream(`/tmp/${repoName}/${key}`);

	return new Promise((resolve, reject) => {
		const pipe = s3.getObject(params).createReadStream().pipe(out);
		pipe.on('error', reject);
		pipe.on('close', resolve);
	});
}

exports.handler = async (event, context, cb) => {
	execSync('rm -rf /tmp/*', { encoding: 'utf8', stdio: 'inherit' });
	execSync(`cd /tmp && git clone --quiet https://${GITLAB_TOKEN_NAME}:${GITLAB_TOKEN}@${GITLAB_REPO_URL}`, { encoding: 'utf8', stdio: 'inherit' });
	
	process.chdir(`/tmp/${repoName}`);

	await getS3Object(event);

	execSync(`git config --local user.email ${GITLAB_EMAIL}`);
  	execSync(`git config --local user.name ${GITLAB_USERNAME}`);
  	execSync('git add .');
  	execSync('git commit -m "commit by :robot:"');
  	execSync('git remote rm origin');
  	execSync(`git remote add origin https://${GITLAB_TOKEN_NAME}:${GITLAB_TOKEN}@${GITLAB_REPO_URL}`);
	
	execSync('git push --porcelain --set-upstream origin master');
	
	cb(null, 'success');
}

