resource "aws_iam_role" "iam_for_lambda111" {
  name = "iam_for_lambda111"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}
resource "aws_s3_bucket" "bucket" {
  bucket = "gabinho7700"
}

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.test_lambda.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.bucket.arn
}

resource "aws_lambda_function" "test_lambda" {
  
  function_name = "hello"
  s3_bucket     = "gabinho770"
  s3_key        = "hello.zip"
  role          =  aws_iam_role.iam_for_lambda111.arn
  handler       = "hello.handler"
  runtime       = "nodejs12.x"
}
resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "gabinho7700"
   lambda_function {
    lambda_function_arn = "arn:aws:lambda:us-west-2:009312466690:function:hello"
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "AWSLogs/"
    filter_suffix       = ".log"
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}